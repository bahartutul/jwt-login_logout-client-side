import { Component, OnInit, Self } from '@angular/core';
import { from } from 'rxjs';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import { AddTodoPopUpComponent } from '../add-todo-pop-up/add-todo-pop-up.component';
import { TodoService } from '../Shared/todo.service';
import { Todo } from '../Shared/todo';
import { ToastrService } from 'ngx-toastr';
import { DateWiseTask } from '../Shared/date-wise-task';

/**************************Year&Month Picker*********************************** */

import {FormControl, NgControl} from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker, MatDatepickerInputEvent} from '@angular/material/datepicker';
 import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
import { TaskComponent } from '../task/task.component';
import { TaskCountByDate } from '../Shared/task-count-by-date';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

/**************************Year&Month Picker*********************************** */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [    
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class HomeComponent implements OnInit {
  todo:Todo;
  taskCount:TaskCountByDate[];
  dateWiseTem:DateWiseTask[]=[];
 //  a=this.todo.date.getDate();
  isChecked:boolean=false;
  id:number;
  now=new Date();
  days:number;
  arr=Array;
  year=this.now.getFullYear();
  month=this.now.getMonth()+1;
  monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December' ];
perticularMonth:string;
perticularYear:string;
 perticularMonthValue:string;

  constructor(private dialog:MatDialog, private todoService:TodoService,private toaster:ToastrService) { }
  date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  
  ngOnInit(): void {  
            
    this.loadTodolist();
    
  }
  // loadFirstTime(){
  //   this.perticularMonthValue=(this.now.getMonth()+1).toString();
  //   this.perticularMonth=  this.monthNames[this.now.getMonth()+1-1];
  //     this.perticularYear=this.now.getFullYear().toString();
  // }
  openDialog(){
    const dialogConf=new MatDialogConfig();
    dialogConf.autoFocus=true;
    dialogConf.disableClose=true;
    dialogConf.width="50%";
   let dialogref= this.dialog.open(AddTodoPopUpComponent,dialogConf);
  dialogref.afterClosed().subscribe(
    res=>{
      this.passMonthYear(localStorage.getItem("passingMonthYear"));
    }
  )
     
   
  }
  loadTodolist(){
    this.perticularMonthValue=(this.now.getMonth()+1).toString();
    this.perticularMonth=  this.monthNames[this.now.getMonth()+1-1];
    this.perticularYear=this.now.getFullYear().toString();

   this.todoService.getAllTask(this.perticularMonthValue,this.perticularYear).subscribe(
     res=>{
       this.taskCount=res as TaskCountByDate[];  
    this.days=new Date(this.now.getFullYear(),this.now.getMonth()+1,0).getDate();
    this.dateWiseTem=[];
    let flag=0;
    let indexCount=0;
   for(let i=1;i<=this.days;i++){
     for(let j=0;j<this.taskCount.length;j++){
       if(i== new Date(this.taskCount[j].date).getDate()){
         flag++;       
        indexCount=j;
        break;
       }else{
         flag=0;
       }
     }
     if(flag>0){
      let test=new DateWiseTask();
      test.count=this.taskCount[indexCount].count;
      test.dateOfMonth=new Date(this.taskCount[indexCount].date).getDate();
      test.date=i;
      test.pendingCount=this.taskCount[indexCount].pendingCount;
      this.dateWiseTem.push(test);
     }else{
      let test=new DateWiseTask();
      test.count=null;
      test.dateOfMonth=new Date(this.taskCount[indexCount].date).getDate();
      test.date=i;
      test.pendingCount=null;
      this.dateWiseTem.push(test);
     }
   }  
     console.log(this.dateWiseTem); 
    
    }
   )  
  }

  checkValue(todo){
  console.log();
   this.todoService.taskAction(todo).subscribe(
     res=>{
     
     }
   );
}
deleteTask(id){
  
  this.todoService.deleteRow(id).subscribe(
    res=>{
      this.toaster.error("1 Row Deleted","Delete Task");
     
    }
  );
}


passMonthYear(monthValue){
  localStorage.setItem("passingMonthYear",monthValue);
  let my=monthValue.split("/");
  this.dateWiseTem=[];
  let flag=0;
  let indexCount=0;
  this.perticularMonthValue=my[0];
  this.perticularMonth=  this.monthNames[my[0]-1];
  this.perticularYear=my[1];
  this.days=new Date(my[1],my[0],0).getDate();

 this.todoService.getAllTask(this.perticularMonthValue,this.perticularYear).subscribe(
   res=>{
     this.taskCount=res as TaskCountByDate[];  
  
  if(this.taskCount.length>0){
 for(let i=1;i<=this.days;i++){
   for(let j=0;j<this.taskCount.length;j++){
     if(i== new Date(this.taskCount[j].date).getDate()){
       flag++;       
      indexCount=j;
      break;
     }else{
       flag=0;
     }
   }
   if(flag>0){
    let test=new DateWiseTask();
    test.count=this.taskCount[indexCount].count;
    // test.dateOfMonth=new Date(this.taskCount[indexCount].date).getDate();
    test.date=i;
    test.pendingCount=this.taskCount[indexCount].pendingCount;
    this.dateWiseTem.push(test);
   }else{
    let test=new DateWiseTask();
    test.count=null;
    // test.dateOfMonth=new Date(this.taskCount[indexCount].date).getDate();
    test.date=i;
    test.pendingCount=null;
    this.dateWiseTem.push(test);
   }
 }  
}
else{
  for(let i=1;i<=this.days;i++){
    let test=new DateWiseTask();
    test.count=null;
    test.dateOfMonth=i;
    this.dateWiseTem.push(test);
  }
}
   console.log(this.dateWiseTem); 
  
  }
 )  
  
}


openTaskDialog(date,month,year){
  
  const dialogConf=new MatDialogConfig();
  dialogConf.autoFocus=true;
  dialogConf.disableClose=true;
  dialogConf.width="50%";
  dialogConf.data={"Date":date,"Month":month,"Year":year};
 let dialogref= this.dialog.open(TaskComponent,dialogConf);
 dialogref.afterClosed().subscribe(
   res=>{
    this.passMonthYear(localStorage.getItem("passingMonthYear"));
   }
 )
}


}

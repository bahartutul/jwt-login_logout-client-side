import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule, ReactiveFormsModule} from '@angular/forms'
import { AppComponent } from './app.component';

import { from } from 'rxjs';
import { SingupService } from 'src/app/Shared/singup.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ToastrModule } from 'ngx-toastr';
import { UserComponent } from './user/user.component';
import { SigninComponent } from './user/signin/signin.component';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { AfterLogComponent } from './user/after-log/after-log.component';
import { SigninService } from './Shared/signin.service';
import { AuthServiceService } from './Shared/auth-service.service';
import { AuthGaurdService } from './Shared/auth-gaurd.service';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';

import { AddTodoPopUpComponent } from './add-todo-pop-up/add-todo-pop-up.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { TaskComponent } from './task/task.component';
@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    UserComponent,
    SigninComponent,
    HomeComponent,
    AfterLogComponent,
    AddTodoPopUpComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    ReactiveFormsModule 
  ],
  providers: [SingupService,SigninService,AuthServiceService,AuthGaurdService, JwtHelperService,{ provide: JWT_OPTIONS, useValue: JWT_OPTIONS }],
  bootstrap: [AppComponent],
  entryComponents:[AddTodoPopUpComponent]
})
export class AppModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Todo } from '../Shared/todo';
import { TodoService } from '../Shared/todo.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  
  task:Todo;
  descript:any;
  perticularMonth:string;
  flag:number;
  monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December' ];
  constructor(private dialogRef: MatDialogRef<TaskComponent>,
    @Inject(MAT_DIALOG_DATA)   data,private todoService:TodoService,private toaster:ToastrService) { 
      this.descript=data;
    }

  ngOnInit(): void {
   this.prticularDateWiseTask();
  }

  prticularDateWiseTask(){
    console.log(this.descript);
   this.todoService.PassDateForTask(this.descript).subscribe(
     res=>{
       
       this.task=res as unknown as Todo
       
       this.perticularMonth=  this.monthNames[this.descript["Month"]-1];
     }
   );
  }
  checkValue(todo){
    console.log();
     this.todoService.taskAction(todo).subscribe(
       res=>{
        this.prticularDateWiseTask();
       }
     );
  }
  deleteTask(id){
  
    this.todoService.deleteRow(id).subscribe(
      res=>{
        this.toaster.error("1 Row Deleted","Delete Task");
        this.prticularDateWiseTask();
      }
    );
  }
  close() {
    this.dialogRef.close();
  }

}

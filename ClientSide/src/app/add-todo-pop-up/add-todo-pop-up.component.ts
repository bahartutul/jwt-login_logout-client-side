import { Component, OnInit,Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA, MatDialogRef
} from "@angular/material/dialog";
import { HomeComponent } from '../home/home.component';
import { TodoService } from '../Shared/todo.service';
@Component({
  selector: 'app-add-todo-pop-up',
  templateUrl: './add-todo-pop-up.component.html',
  styleUrls: ['./add-todo-pop-up.component.css']
})
export class AddTodoPopUpComponent implements OnInit {
  startDate = new Date(2015, 0, 1);
  loggedUser:number;
  constructor(@Inject(MAT_DIALOG_DATA) public data:any,public dialogref:MatDialogRef<AddTodoPopUpComponent>,
  private todoService:TodoService) { }

  ngOnInit(): void {
  }
  addTask(inputText,selectedDate){
    this.loggedUser=Number(localStorage.getItem('loggedUser'));
    console.log(inputText,selectedDate,this.loggedUser);
   this.todoService.addDailyTask(inputText,selectedDate,this.loggedUser).subscribe(
     res=>{
      this.close();
     }
   );
   
  }
  close() {
    this.dialogref.close();
  }

  

}

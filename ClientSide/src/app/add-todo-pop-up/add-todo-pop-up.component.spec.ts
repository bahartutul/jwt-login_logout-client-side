import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTodoPopUpComponent } from './add-todo-pop-up.component';

describe('AddTodoPopUpComponent', () => {
  let component: AddTodoPopUpComponent;
  let fixture: ComponentFixture<AddTodoPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTodoPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTodoPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Shared/user.class';
import { NgForm,Validator } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ThrowStmt } from '@angular/compiler';
import { SingupService } from 'src/app/Shared/singup.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user:User;
  isValid:boolean=true;
  constructor(private service: SingupService,private tostar:ToastrService) { }

  ngOnInit(): void {
    this.resetForm();
  }
   resetForm(form?:NgForm){
     
     if(form!=null){
       form.reset();
     }
     this.user={
       username:'',
       password:'',
       email:'',
       firstName:'',
       lastName:'',
       userId:0
     }
   }
formValidation(formdata:User){
  this.isValid=true;
  if(formdata.username==""){
    this.isValid=false;
  }
  if(formdata.password==''){
    this.isValid=false;
  }
  if(formdata.email==''){
    this.isValid=false;
  }
  if(formdata.firstName==''){
    this.isValid=false;
  }
  if(formdata.lastName==''){
    this.isValid=false;
  }

  return this.isValid;
}
   ValuePass(form:NgForm){
     console.log(form.value+"---"+this.formValidation(form.value));
    if(this.formValidation(form.value)){
    this.service.PassFormValue(form.value).subscribe(
      res=>{
        this.tostar.success("Registration Successfully Done...","User Registration")
      },
      err=>{}
    )
   }
  }
}

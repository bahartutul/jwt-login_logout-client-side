import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { SigninService } from 'src/app/Shared/signin.service';
import { LogedUser } from 'src/app/Shared/loged-user';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
user:LogedUser
  constructor(private service:SigninService,private toastr:ToastrService,private router:Router) { }

  ngOnInit(): void {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null){
      form.reset();
    }
    this.user={
      username:'',
      password:'',
      
    }
  }

  onSubmit(form:NgForm){
   this.service.login(form.value).subscribe(
     res=>{
      console.log(res);
       if(res){

        console.log("Login:"+res['userid'])
        localStorage.setItem('token',res['token']);
        localStorage.setItem('loggedUser',res['userid']);
        this.toastr.success("Successfully Signed In","SIGN IN")
        this.router.navigate(['Home']);
       }
       
        
     
        
     },
     err=>{
      this.toastr.error("UserName & Password not Matched","SIGN IN")
     }
   )
  }

}

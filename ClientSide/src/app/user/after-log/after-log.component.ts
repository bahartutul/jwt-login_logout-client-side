import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-after-log',
  templateUrl: './after-log.component.html',
  styleUrls: ['./after-log.component.css']
})
export class AfterLogComponent implements OnInit {
 rootUrl="https://localhost:44395/api/";
arrayofvalues:[];
//  headersApi = new HttpHeaders();
  constructor(private http:HttpClient,private toastr:ToastrService) { }

  ngOnInit(): void {
     this.getallItem();
  }
  getallItem(){
    
   let headersApi = new HttpHeaders({'Authorization':'Bearer ' +localStorage.getItem('token')});
    this.http.get(this.rootUrl+"values",{ headers: headersApi}).subscribe(
      res=>{
     this.arrayofvalues=res as []
     console.log(this.arrayofvalues);
      },
      err=>{
          if(err.status==401){
            
            localStorage.removeItem('token');
            this.toastr.warning("Token Time Out, Reload the Page for login");

          }
      }
    )
  }

}

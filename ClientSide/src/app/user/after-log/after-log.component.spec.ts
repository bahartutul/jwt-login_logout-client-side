import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterLogComponent } from './after-log.component';

describe('AfterLogComponent', () => {
  let component: AfterLogComponent;
  let fixture: ComponentFixture<AfterLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfterLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

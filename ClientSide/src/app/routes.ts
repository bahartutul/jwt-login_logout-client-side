import{Routes} from '@angular/router'
import { from } from 'rxjs'
import { HomeComponent } from './home/home.component'
import { SignUpComponent } from './user/sign-up/sign-up.component'
import { UserComponent } from './user/user.component'
import { SigninComponent } from './user/signin/signin.component'

import { AuthGaurdService } from './Shared/auth-gaurd.service'
import { AfterLogComponent } from './user/after-log/after-log.component'
import { AuthServiceService } from './Shared/auth-service.service'
export const appRoutes:Routes=[
{path:'Home', component:HomeComponent,canActivate:[AuthGaurdService]},
{path:'afterLog', component:AfterLogComponent,canActivate:[AuthGaurdService]},
{
    path:'signUp',component:UserComponent,
    children:[{path:'',component:SignUpComponent}]
},
{
    path:'login',component:UserComponent,
    children:[{path:'',component:SigninComponent}]
},

{path:'',redirectTo:'/login',pathMatch:'full'}

]
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user.class';
import { LogedUser } from './loged-user';

@Injectable({
  providedIn: 'root'
})
export class SigninService {
userLoginParameter:LogedUser
rootUrl="https://localhost:44395/api/"
  constructor(private http:HttpClient) { }
 
  login(userLoginParameter){
    console.log(userLoginParameter);
  return this.http.post(this.rootUrl+"Login",userLoginParameter);
  }
}

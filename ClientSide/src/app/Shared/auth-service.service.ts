import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private jwtHelper:JwtHelperService,private _router:Router) { }

  public isAuthenticated():boolean{
   const token=localStorage.getItem('token');
   console.log(token);
 return this.jwtHelper.isTokenExpired(token);
  }
}

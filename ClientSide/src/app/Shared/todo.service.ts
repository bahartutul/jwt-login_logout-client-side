import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
 rootUrl="https://localhost:44395/api/";
 todo:Todo
 id:string
  constructor( private http:HttpClient) { }
  addDailyTask(value,selectedDate,loggedUser){
 this.todo=new Todo();
console.log("--------------"+loggedUser);
 this.todo.task=value;
 this.todo.date=selectedDate;
 this.todo.assignTo=loggedUser;
return this.http.post(this.rootUrl+"Todoes",this.todo);
  }

  getAllTask(month,year){
    return this.http.get(this.rootUrl+"Todoes/"+month+"/"+year);
  }

  taskAction(todo){
    return this.http.put(this.rootUrl+"Todoes",todo);
  }
  deleteRow(id){
   
    return this.http.delete(this.rootUrl+"Todoes/"+id);
  }
  
  PassDateForTask(searchBy){ 
  
    return this.http.get(this.rootUrl+"Todoes/"+searchBy["Date"]+"/"+searchBy["Month"]+"/"+searchBy["Year"]);
   
  }
}

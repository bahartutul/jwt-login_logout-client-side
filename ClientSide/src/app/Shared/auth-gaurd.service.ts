import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService implements CanActivate {

  constructor(public auth:AuthServiceService,private router:Router) { }
canActivate():boolean{

  
  if(localStorage.getItem('token')){
    
    
    return true;
  }
  this.router.navigate(['']);
  return false;
 
}
}

export class Todo {
    todoId:number;
    task:string;
    date:Date;
    isChecked:boolean;
    assignTo:number;
    
}
